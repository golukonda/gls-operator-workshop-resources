# myapp/forms.py
from flask_wtf import FlaskForm
from wtforms import StringField,BooleanField, SubmitField
from wtforms.validators import DataRequired

class SubscribeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    submit = SubmitField("Subscribe")
